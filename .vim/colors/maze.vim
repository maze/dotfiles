" Created by Michael Zeevi based on the templates from https://vimcolors.org
" version: 1.1

hi clear
syntax reset
let g:colors_name="maze"
set background=dark
set t_Co=256

hi Normal guifg=#FFFFFF ctermbg=NONE guibg=#111111 gui=NONE cterm=NONE
hi DiffText guifg=#F2E64A guibg=NONE
hi ErrorMsg guifg=#ED9F4F guibg=NONE
hi WarningMsg guifg=#ED9F4F guibg=NONE
hi PreProc guifg=#FFFFFF guibg=NONE
hi Exception guifg=#FD3F42 guibg=NONE
hi Error guifg=#ED9F4F guibg=NONE
hi DiffDelete guifg=#FD3F42 guibg=NONE
hi GitGutterDelete guifg=#FD3F42 guibg=NONE
hi GitGutterChangeDelete guifg=#F2E64A guibg=NONE
hi cssIdentifier guifg=#F24AAA guibg=NONE
hi cssImportant guifg=#F24AAA guibg=NONE
hi Type guifg=#FD3F42 guibg=NONE
hi Identifier guifg=#FD3F42 guibg=NONE
hi PMenuSel guifg=#46D0F6 guibg=NONE
hi Constant guifg=#46D0F6 guibg=NONE
hi Repeat guifg=#46D0F6 guibg=NONE
hi DiffAdd guifg=#95F547 guibg=NONE
hi GitGutterAdd guifg=#46D0F6 guibg=NONE
hi cssIncludeKeyword guifg=#46D0F6 guibg=NONE
hi Keyword guifg=#46D0F6 guibg=NONE
hi IncSearch guifg=#F2E64A guibg=NONE
hi Title guifg=#F2E64A guibg=NONE
hi PreCondit guifg=#F2E64A guibg=NONE
hi Debug guifg=#F2E64A guibg=NONE
hi SpecialChar guifg=#F2E64A guibg=NONE
hi Conditional guifg=#F2E64A guibg=NONE
hi Todo guifg=#111111 guibg=#F24AAA
hi Special guifg=#F2E64A guibg=NONE
hi Label guifg=#F2E64A guibg=NONE
hi Delimiter guifg=#F2E64A guibg=NONE
hi Number guifg=#46D0F6 guibg=NONE
hi CursorLineNR guifg=#F24AAA guibg=NONE
hi Define guifg=#F2E64A guibg=NONE
hi MoreMsg guifg=#F2E64A guibg=NONE
hi Tag guifg=#F2E64A guibg=NONE
hi String guifg=#F2E64A guibg=NONE
hi MatchParen guifg=#F2E64A guibg=NONE
hi Macro guifg=#F2E64A guibg=NONE
hi DiffChange guifg=#F2E64A guibg=NONE
hi GitGutterChange guifg=#F2E64A guibg=NONE
hi cssColor guifg=#F2E64A guibg=NONE
hi Function guifg=#95F547 guibg=NONE
hi Directory guifg=#46D0F6 guibg=NONE
hi markdownLinkText guifg=#FD3F42 guibg=NONE
hi javaScriptBoolean guifg=#46D0F6 guibg=NONE
hi Include guifg=#FD3F42 guibg=NONE
hi Storage guifg=#FD3F42 guibg=NONE
hi cssClassName guifg=#FD3F42 guibg=NONE
hi cssClassNameDot guifg=#FD3F42 guibg=NONE
hi Statement guifg=#FFFFFF guibg=NONE
hi Operator guifg=#FFFFFF guibg=NONE
hi cssAttr guifg=#FFFFFF guibg=NONE
hi Pmenu guifg=#FFFFFF guibg=#252525
hi SignColumn guibg=#111111
hi Title guifg=#F2E64A
hi LineNr guifg=#707070 guibg=#111111
hi NonText guifg=#707070 guibg=#111111
hi Comment guifg=#707070 gui=italic cterm=italic
hi SpecialComment guifg=#707070 guibg=NONE gui=italic cterm=italic
hi CursorLine guifg=NONE guibg=#333333 gui=NONE cterm=NONE
hi TabLineFill gui=NONE cterm=NONE guibg=#252525
hi TabLine guifg=#707070 guibg=#252525 gui=NONE
hi StatusLine gui=bold cterm=bold guibg=#252525 guifg=#FFFFFF
hi StatusLineNC gui=NONE guibg=#111111 guifg=#FFFFFF
hi Search guibg=#F24AAA guifg=#FFFFFF
hi VertSplit gui=NONE guifg=#292929 guibg=NONE
hi Visual gui=NONE guibg=#555555


hi SpellBad   guisp=red    gui=undercurl term=underline cterm=underline
hi SpellCap   guisp=yellow gui=undercurl guifg=NONE guibg=NONE ctermfg=NONE ctermbg=NONE term=underline cterm=underline
hi SpellRare  guisp=blue   gui=undercurl guifg=NONE guibg=NONE ctermfg=NONE ctermbg=NONE term=underline cterm=underline
hi SpellLocal guisp=orange gui=undercurl guifg=NONE guibg=NONE ctermfg=NONE ctermbg=NONE term=underline cterm=underline
