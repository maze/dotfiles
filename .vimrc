" line numbers and coordinates
set number
set relativenumber
set ruler

" tabs and indention
set tabstop=2
set softtabstop=2
set shiftwidth=2
set autoindent
set expandtab

" search
set hlsearch
set incsearch
set ignorecase
set smartcase

" colors and syntax
" favorites: badwolf/maze/sublimemonokai
colorscheme maze
set background=dark
set termguicolors
syntax enable
set list
set listchars=
set listchars+=tab:»·
set listchars+=trail:·
autocmd FileType gitconfig,make set noexpandtab
" filetype associations
autocmd BufRead .vimrc                    setfiletype vim
autocmd BufRead ~/*kube*config            setfiletype yaml
autocmd BufRead,BufNewFile .*rc           setfiletype bash
autocmd BufRead,BufNewFile *ockerfile*    setfiletype dockerfile
autocmd BufRead,BufNewFile *enkinsfile*   setfiletype groovy
autocmd BufRead,BufNewFile todo,quicknote setfiletype markdown
" fix spellcheck not highlighting in some colorschemes
autocmd VimEnter,ColorScheme,BufReadPost * highlight SpellBad cterm=underline ctermbg=1 ctermfg=0 guibg=#ff3333 guifg=#111111

" prevent bell and error sounds
set noerrorbells
set visualbell
set t_vb=

" misc
autocmd BufWritePost $MYVIMRC source $MYVIMRC
set title
set wildmenu
set scrolloff=5
set updatetime=500
set foldmethod=manual
set foldlevel=99
set cursorline
" jump to the last position when reopening a file
autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" key mappings
inoremap jj <Esc>
nnoremap Y y$
nnoremap H :nohlsearch<CR>
nnoremap <F7>      :setlocal spell!<CR>
inoremap <F7> <C-o>:setlocal spell!<CR>
nnoremap <silent> <F5> :setlocal nospell<CR>:nohlsearch<CR>
" search and replace all instances of current word under cursor
nnoremap S *:%s///g<Left><Left>
" move current line or selection up/down
nnoremap <silent> <C-k> :m -2<CR>
nnoremap <silent> <C-j> :m +1<CR>
vnoremap <silent> <C-k> :m '<-2<CR>gv
vnoremap <silent> <C-j> :m '>+1<CR>gv
" adjust indentation
nnoremap <C-h> <<
nnoremap <C-l> >>
vnoremap <C-h> <gv
vnoremap <C-l> >gv
" cover for commands typos
command W w
command Q q
command Wq wq
" leader mappings
let mapleader = ','
nnoremap <leader>v :e $MYVIMRC<CR>
nnoremap <leader>c :colorscheme random<CR>
nnoremap <leader>n :set number! relativenumber!<CR>
nnoremap <leader>l :set cursorline!<CR>
nnoremap <leader>w :set wrap!<CR>
nnoremap <leader>s :setlocal spell!<CR>
nnoremap <leader>b :let &background=(&background == 'dark' ? 'light' : 'dark')<CR>
nnoremap <leader>m :w<CR>:make<CR><CR><CR>
nnoremap <leader>t /TODO<CR>
nnoremap <leader>dc :s/\v *(#\|\/\/) ?.+$//<CR>:noh<CR>  " deletes python/java comments (`#` or `//`)
nnoremap <expr> <leader>f &foldlevel ? 'zM' :'zR'
" disable certain keys
map Q <nop>
map q <nop>

" install plugin manager and auto install any new plugins
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)')) | PlugInstall --sync | source $MYVIMRC | endif

" plugin downloads
call plug#begin()
  Plug 'tpope/vim-commentary'
  Plug 'airblade/vim-gitgutter'
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'junegunn/fzf.vim'
  " Plug 'tpope/vim-surround'  " awaiting merge of my fork to upstream: https://github.com/tpope/vim-surround/pull/378
  Plug 'https://codeberg.org/maze/vim-surround'
  Plug 'ap/vim-css-color'
  Plug 'hashivim/vim-terraform'
  Plug 'towolf/vim-helm'
  Plug 'chr4/nginx.vim'
call plug#end()

" plugin settings: vim-commentary
if has_key(plugs, 'vim-commentary')
  nnoremap <silent> <C-_> :Commentary<CR>
  vnoremap <silent> <C-_> :Commentary<CR>gv
endif

" plugin settings: gitgutter
if has_key(plugs, 'vim-gitgutter')
  nmap     <F6>      :GitGutterToggle<CR>,n
  nnoremap <leader>g :GitGutterToggle<CR>
  autocmd VimEnter,ColorScheme * highlight clear SignColumn
  autocmd VimEnter,ColorScheme * highlight GitGutterDelete ctermfg=1 guifg=#ff3333
  autocmd VimEnter,ColorScheme * highlight GitGutterAdd    ctermfg=2 guifg=#66ff00
  autocmd VimEnter,ColorScheme * highlight GitGutterChange ctermfg=3 guifg=#ffee44
endif

" plugin settings: fzf
if has_key(plugs, 'fzf.vim')
  nnoremap <C-p> :Files<CR>
  nnoremap <C-b> :Buffers<CR>
  nnoremap <C-f> :Lines<CR>
endif

" plugin settings: vim-surround
if has_key(plugs, 'vim-surround')
  " the following setting requires my fork of the vim-surround plugin: https://codeberg.org/maze/vim-surround
  let g:surround_insert_space = 0
  " can also visual surround with: vS*
endif

" plugin settings: vim-css-color
if has_key(plugs, 'vim-css-color')
  nnoremap <leader>C :call css_color#toggle()<CR>
endif
